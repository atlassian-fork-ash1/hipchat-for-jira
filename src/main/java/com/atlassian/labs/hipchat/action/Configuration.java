package com.atlassian.labs.hipchat.action;

import com.atlassian.fugue.Option;
import com.atlassian.hipchat.plugins.api.client.ClientError;
import com.atlassian.hipchat.plugins.api.client.HipChatClient;
import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Function;
import com.google.common.base.Strings;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

@WebSudoRequired
public class Configuration extends JiraWebActionSupport
{
    private final HipChatConfigurationManager configurationManager;
    private final HipChatClient hipChatClient;

    private String hipChatAuthToken;
    private String hipChatApiBaseUrl;

    private boolean btf;
    private boolean success;

    public Configuration(HipChatConfigurationManager configurationManager, HipChatClient hipChatClient) {
        this.configurationManager = checkNotNull(configurationManager);
        this.hipChatClient = hipChatClient;
    }

    public void setHipChatAuthToken(String hipChatAuthToken) {
        this.hipChatAuthToken = hipChatAuthToken;
    }

    public String getHipChatAuthToken() {
        if (hipChatAuthToken != null)
            return hipChatAuthToken;
        return getFakeHipChatAuthToken();
    }

    public String getHipChatApiBaseUrl()
    {
        return option(hipChatApiBaseUrl).getOrElse(configurationManager.getApiBaseUrl());
    }

    public void setHipChatApiBaseUrl(String hipChatApiBaseUrl)
    {
        this.hipChatApiBaseUrl = hipChatApiBaseUrl;
    }

    public String getFakeHipChatAuthToken() {
        return Strings.repeat("#", configurationManager.getApiToken().getOrElse("").length());
    }

    public boolean isBtfOrDevMode()
    {
        return btf || Boolean.getBoolean("atlassian.dev.mode");
    }

    public void setBtf(final boolean btf)
    {
        this.btf = btf;
    }

    @Override
    protected void doValidation()
    {
        final String originalBaseUrl = configurationManager.getApiBaseUrl();
        try
        {
            // test against the supplied api base url
            configurationManager.setApiBaseUrl(getHipChatApiBaseUrl());
            if (!getFakeHipChatAuthToken().equals(getHipChatAuthToken()))
            {
                hipChatClient.canAuthenticate(Option.<String>some(getHipChatAuthToken())).claim().<Option<String>>fold(
                        new Function<ClientError, Option<String>>()
                        {
                            @Override
                            public Option<String> apply(ClientError error)
                            {
                                return some(error.getMessage());
                            }
                        },
                        new Function<Boolean, Option<String>>()
                        {
                            @Override
                            public Option<String> apply(Boolean b)
                            {
                                return b ? Option.<String>none() : some(getText("hipchat.admin.invalid.token"));
                            }
                        })
                        .map(new Function<String, Object>()
                        {
                            @Override
                            public Void apply(String message)
                            {
                                addErrorMessage(message);
                                return null;
                            }
                        });
            }
        }
        finally
        {
            configurationManager.setApiBaseUrl(originalBaseUrl);
        }
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        // only change the token if this is a real update
        configurationManager.setApiBaseUrl(getHipChatApiBaseUrl());
        if (!getFakeHipChatAuthToken().equals(getHipChatAuthToken())) {
            configurationManager.setApiToken(getHipChatAuthToken());
            setHipChatAuthToken(null);
            success = true;
        }
        return SUCCESS;
    }

    public boolean isSuccess() {
        return success;
    }

}