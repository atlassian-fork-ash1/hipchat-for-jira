package com.atlassian.labs.jira.rest;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.workflow.WorkflowService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.JiraWorkflow;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("workflow")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class HipChatWorkflowResource
{
    private final JiraServiceContext jiraServiceContext;
    private final WorkflowService workflowService;

    public HipChatWorkflowResource(final WorkflowService workflowService, final JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.jiraServiceContext = new JiraServiceContextImpl(jiraAuthenticationContext.getUser(), new SimpleErrorCollection());
        this.workflowService = workflowService;
    }

    @POST
    @Path("createDraft")
    public Response createDraftWorkflow(final String workflowName)
    {
        final JiraWorkflow workflow = workflowService.getDraftWorkflow(jiraServiceContext, workflowName);
        if (workflow == null)
        {
            workflowService.createDraftWorkflow(jiraServiceContext, workflowName);
        }
        return Response.ok().build();
    }

}
