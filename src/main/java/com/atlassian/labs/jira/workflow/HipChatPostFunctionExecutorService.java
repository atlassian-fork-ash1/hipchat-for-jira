package com.atlassian.labs.jira.workflow;

import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.annotation.PreDestroy;

public class HipChatPostFunctionExecutorService
{
    private final ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory;
    private final ExecutorService threadLocalExecutor;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public HipChatPostFunctionExecutorService(final ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory)
    {
        this.threadLocalDelegateExecutorFactory = threadLocalDelegateExecutorFactory;
        this.threadLocalExecutor = threadLocalDelegateExecutorFactory.createExecutorService(
                Executors.newCachedThreadPool(new ThreadFactoryBuilder().setNameFormat(getClass().getSimpleName() + "-pool-thread-%d").build()));
    }

    public void execute(final Runnable runnable)
    {
        threadLocalExecutor.execute(threadLocalDelegateExecutorFactory.createRunnable(runnable));
    }

    @PreDestroy
    public void destroy()
    {
        logger.debug("Shutting down HipChat post function executor service");
        threadLocalExecutor.shutdown();
    }
}
