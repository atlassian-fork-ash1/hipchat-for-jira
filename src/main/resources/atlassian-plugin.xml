<?xml version="1.0" encoding="UTF-8"?>

<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
        <param name="configure.url">/secure/admin/HipChatConfiguration!default.jspa</param>
    </plugin-info>
    <resource type="i18n" name="i18n" location="atlassian-plugin"/>
    <component-import key="template-renderer"
                      interface="com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer"/>
    <component-import key="application-properties" interface="com.atlassian.sal.api.ApplicationProperties"/>

    <component name="Legacy Configuration Manager" key="legacyConfigurationManager"
               class="com.atlassian.labs.hipchat.components.ConfigurationManagerImpl" public="true">
        <description>Stores the details of the configured HipChat settings in SAL</description>
        <interface>com.atlassian.labs.hipchat.components.ConfigurationManager</interface>
    </component>

    <component-import key="configurationManager" interface="com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager"/>
    <component-import key="hipChatClient" interface="com.atlassian.hipchat.plugins.api.client.HipChatClient" />

    <component-import name="Request Factory" key="request-factory"
                      interface="com.atlassian.sal.api.net.RequestFactory"/>
    <component-import name="Thread Local Delegate Executor Factory" key="thread-local-delegate-executor-factory"
                      interface="com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory"/>

    <component name="HipChat Post Function Executor Service" key="hipchat-post-function-executor-service"
               class="com.atlassian.labs.jira.workflow.HipChatPostFunctionExecutorService" />

    <component name="HipChat Message Renderer" key="hipchat-message-renderer"
               class="com.atlassian.labs.jira.notification.HipChatMessageRenderer" />

    <web-resource name="Resources" key="resources">
        <resource name="status.js" type="download" location="js/status.js"/>
        <resource name="styles.css" type="download" location="css/styles.css"/>
        <resource type="download" name="images/" location="images/"/>
        <context>jira.general</context>
        <condition class="com.atlassian.hipchat.plugins.api.IsHipChatApiTokenConfiguredCondition"
                   class2="com.atlassian.hipchat.plugins.api.UrlReadingIsHipChatApiTokenConfiguredCondition"/>
    </web-resource>

    <web-resource name="HipchatRoomPicker" key="hipchatRoomPicker">
        <resource name="initHipchatRoomPickers.js" type="download" location="js/initHipchatRoomPickers.js"/>
        <context>atl.admin</context>
        <dependency>jira.webresources:select-pickers</dependency>
        <condition class="com.atlassian.hipchat.plugins.api.IsHipChatApiTokenConfiguredCondition"/>
    </web-resource>

    <web-resource name="Notification JQL Resources" key="notification-jql-resources">
        <resource name="jql-autocomplete.js" type="download" location="js/jql-autocomplete.js"/>
        <resource name="jql-autocomplete.css" type="download" location="css/jql-autocomplete.css"/>

        <context>atl.admin</context>
        <dependency>jira.webresources:jqlautocomplete</dependency>
    </web-resource>

    <web-resource name="Notification Preview Resources" key="notification-preview-resources">
        <resource name="notification-preview.js" type="download" location="js/notification-preview.js"/>
        <resource name="notification-preview.css" type="download" location="css/notification-preview.css"/>
        <resource name="notification-preview-template.js" type="download" location="templates/notification-preview.soy"/>

        <resource name="notification-editor.js" type="download" location="js/notification-editor.js"/>
        <resource name="notification-editor.css" type="download" location="css/notification-editor.css"/>

        <transformation extension="soy">
            <transformer key="jiraSoyTransformer"/>
        </transformation>

        <context>atl.admin</context>
    </web-resource>

    <webwork1 key="hip-chat-actions" class="java.lang.Object">
        <actions>
            <action name="com.atlassian.labs.hipchat.action.Configuration"
                    alias="HipChatConfiguration">
                <view name="success">/templates/hip-chat-admin.vm</view>
                <view name="input">/templates/hip-chat-admin.vm</view>
            </action>
        </actions>
    </webwork1>

    <web-item name="HipChat Configuration" i18n-name-key="hipchat.config" key="hipchat-config"
              section="top_system_section/mail_section" weight="150" application="jira">
        <description key="hipchat.config">HipChat Configuration</description>
        <label key="hipchat.config">HipChat Configuration</label>
        <link linkId="hipchat.config.link">/secure/admin/HipChatConfiguration!default.jspa</link>
    </web-item>
    <workflow-function key="hip-chat-post-function" name="HipChat Post Function"
                       i18n-name-key="hip-chat-post-function.name"
                       class="com.atlassian.labs.jira.workflow.HipChatPostFunctionFactory">
        <description key="hip-chat-post-function.description">The Hip Chat Post Function Plugin</description>
        <function-class>com.atlassian.labs.jira.workflow.HipChatPostFunction</function-class>
        <resource type="velocity" name="view" location="templates/postfunctions/hip-chat-post-function.vm"/>
        <resource type="velocity" name="input-parameters"
                  location="templates/postfunctions/hip-chat-post-function-input.vm"/>
        <resource type="velocity" name="edit-parameters"
                  location="templates/postfunctions/hip-chat-post-function-input.vm"/>
    </workflow-function>

    <!-- HipChat Notifications Menu -->
    <web-resource name="HipChat Web Panel Resources" key="hipchat-web-panel-resources">
        <resource name="hipchat-web-panel-styles.css" type="download" location="css/hipchat-web-panel-styles.css"/>
        <resource name="images/" type="download" location="images/"/>
        <context>hipchat-webpanel-context</context>
    </web-resource>

    <web-resource name="HipChat Notifications Page Resources" key="hipchat-notifications-page-resources">
        <resource name="hipchat-notifications-page-styles.css" type="download" location="css/hipchat-notifications-page-styles.css"/>
        <resource name="hipchatNotificationsConfig.js" type="download" location="js/hipchatNotificationsConfig.js"/>
        <resource name="images/" type="download" location="images/"/>
        <context>hipchat-notifications-page-context</context>
    </web-resource>

    <servlet key="hipchat-notifications-servlet" class="com.atlassian.labs.hipchat.servlet.HipChatNotificationsServlet">
        <url-pattern>/hipchat/notifications/*</url-pattern>
    </servlet>

    <web-item name="HipChat Notifications" i18n-name-key="hipchat.notifications.link" key="hipchat-notifications" weight="50"
              section="atl.jira.proj.config/projectgroup4">
        <label key="hipchat.notifications.link">HipChat</label>
        <link linkId="hipchat.notifications.link">/plugins/servlet/hipchat/notifications/$projectKeyEncoded</link>
    </web-item>

    <web-panel key="summary-hipchat" location="webpanels.admin.summary.right-panels" weight="185">
        <resource name="view" type="velocity" location="templates/hip-chat-web-panel.vm"/>
        <resource name="styles.css" type="download" location="css/styles.css"/>
        <context-provider class="com.atlassian.labs.hipchat.contextproviders.HipChatWebPanelContextProvider"/>
        <label key="hipchat.notifications.title"/>
    </web-panel>

    <rest key="hipchat-for-jira-rest" path="/hipchat" version="1.0">
        <package>com.atlassian.labs.jira.rest</package>
    </rest>

</atlassian-plugin>
