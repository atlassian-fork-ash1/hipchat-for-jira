(function($) {

    function showLoading() {
        $(".hipchat-jql-loading").show();
    }

    function hideLoading() {
        $(".hipchat-jql-loading").hide();
    }

    function getJqlAutoCompleteData() {
        return $.ajax({
            url: AJS.contextPath() + "/rest/api/2/jql/autocompletedata",
            type: "GET"
        });
    }

    function init(options) {

        showLoading();
        getJqlAutoCompleteData()
            .done(function(autoCompleteData) {
                var fieldID = options.fieldID;
                var errorID = options.errorID;

                var $field = $('#'+fieldID);

                var jqlFieldNames = autoCompleteData.visibleFieldNames || [];
                var jqlFunctionNames = autoCompleteData.visibleFunctionNames || [];
                var jqlReservedWords = autoCompleteData.jqlReservedWords || [];

                var jqlAutoComplete = JIRA.JQLAutoComplete({
                    fieldID: fieldID,
                    parser: JIRA.JQLAutoComplete.MyParser(jqlReservedWords),
                    queryDelay: .65,
                    jqlFieldNames: jqlFieldNames,
                    jqlFunctionNames: jqlFunctionNames,
                    minQueryLength: 0,
                    allowArrowCarousel: true,
                    autoSelectFirst: false,
                    errorID: errorID
                });
            })
            .always(function() {
                hideLoading();
            });
    }

    AJS.toInit(function() {
        var $input = $("#hipchat-jql-text");
        if ($input.length) {
            init({
                fieldID: "hipchat-jql-text",
                errorID: "hipchat-jql-error"
            });
        }
    });
})(AJS.$);