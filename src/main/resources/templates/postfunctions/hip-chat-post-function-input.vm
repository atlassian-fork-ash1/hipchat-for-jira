#* @vtlvariable name="i18n" type="com.atlassian.jira.util.I18nHelper" *#
#* @vtlvariable name="baseUrl" type="java.lang.String" *#
#* @vtlvariable name="dto" type="com.atlassian.labs.jira.workflow.HipChatPostFunctionFactory.EditDto" *#

${webResourceManager.requireResource("com.atlassian.labs.hipchat.hipchat-for-jira-plugin:hipchatRoomPicker")}

<script type="text/javascript">
    // Ideally we should make this change in JIRA, but for now this will do
    AJS.toInit(function($) {
        $("form[action='AddWorkflowTransitionFunctionParams.jspa']").addClass("aui");
        $("form[action='EditWorkflowTransitionPostFunctionParams.jspa']").addClass("aui");
    });
</script>

<tr>
    <td colspan="2">
        #if(!$dto.apiTokenConfigured)
            $i18n.getText("hipchat.postfunction.tokennotconfigured", "$baseUrl/secure/admin/HipChatConfiguration!default.jspa")
        #end

        #if($dto.responseError)
            $i18n.getText("hipchat.postfunction.requesterror", "$baseUrl/secure/admin/HipChatConfiguration!default.jspa")
        #end
    </td>
</tr>
#if (!$dto.apiTokenConfigured || $dto.responseError)
<style>
        /* quick fix to hide add button on error */
    #add_submit, #update_submit {
        display: none;
    }
</style>
#else
<tr>
    <td colspan="2">$i18n.getText("hipchat.postfunctionedit.jqlintro")</td>
</tr>
<tr>
    <td class="fieldLabelArea">
        <span class="icon loading hipchat-jql-loading" style="display: none;"></span>
        $i18n.getText("hipchat.postfunctionedit.jql")
    </td>
    <td nowrap>
        <div class="hipchat-jql-container">
            <span id="hipchat-jql-error" class="icon #if ($dto.jqlValid)jqlgood #else jqlerror #end"></span>
            <textarea class="textarea long-field" name="jql" id="hipchat-jql-text" rows="3">$textutils.htmlEncode($!dto.jql)</textarea>
        </div>
    </td>
</tr>
<tr>
    <td colspan="2">$i18n.getText("hipchat.postfunctionedit.roomsintro")</td>
</tr>
<tr>
    <td class="fieldLabelArea">
        $i18n.getText("hipchat.postfunctionedit.roomslistlabel")
    </td>
    <td nowrap>
        <select id="hipchatRoomSelect" name="roomsToNotify" class="hidden" multiple="true">
            #foreach($room in $dto.rooms)
            <option value="$room.id" #if($dto.roomsToNotifyIds.contains($room.id)) selected="selected" #end>$textutils.htmlEncode($room.name)</option>
            #end
        </select>
    </td>
</tr>
<tr>
    <td colspan="2">$i18n.getText("hipchat.postfunctionedit.notifyclientsintro")</td>
</tr>
<tr>
    <td class="fieldLabelArea">
    </td>
    <td nowrap>
        <input type="checkbox" class="checkbox" id="notifyClients" name="notifyClients" value="true"
                    #if($dto.notifyClients) checked #end>
        <label for="notifyClients">$i18n.getText("hipchat.postfunctionedit.notifyclientsenabled")</label>
    </td>
</tr>
<tr>
    <td colspan="2">$i18n.getText("hipchat.notification")</td>
</tr>
<tr>
    <td class="fieldLabelArea">
        $i18n.getText("hipchat.notification.message")
    </td>
    <td nowrap>
        <div class="hipchat-notification-editor-toolbar">
            <div class="aui-buttons">
                <div class="aui-button" data-macro="issue" title="$i18n.getText('hipchat.notification.macro.issue.desc')">$i18n.getText("common.concepts.issue")</div>
                <div class="aui-button" data-macro="issueKey" title="$i18n.getText('hipchat.notification.macro.key.desc')">$i18n.getText("issue.field.key")</div>
                <div class="aui-button" data-macro="issueSummary" title="$i18n.getText('hipchat.notification.macro.summary.desc')">$i18n.getText("issue.field.summary")</div>
                <div class="aui-button" data-macro="issueType" title="$i18n.getText('hipchat.notification.macro.type.desc')">$i18n.getText("issue.field.type")</div>
                <div class="aui-button" data-macro="assignee" title="$i18n.getText('hipchat.notification.macro.assignee.desc')">$i18n.getText("issue.field.assignee")</div>
                <div class="aui-button" data-macro="user" title="$i18n.getText('hipchat.notification.macro.user.desc')">$i18n.getText("common.words.user")</div>
            </div>
        </div>
        <textarea name="message" id="message" rows="3" class="textarea long-field hipchat-notification-editor hipchat-notification-preview-source">$textutils.htmlEncode($!dto.message)</textarea>
    </td>
</tr>
<tr>
    <td class="fieldLabelArea">
        $i18n.getText("hipchat.notification.preview")
    </td>
    <td nowrap>
        <div class="hipchat-notification-preview"></div>
    </td>
</tr>
#end
