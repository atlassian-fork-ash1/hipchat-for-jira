package it.com.atlassian.labs.hipchat.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class HipChatConfigPage extends AbstractJiraPage
{
    private final static String URI = "/secure/admin/HipChatConfiguration!default.jspa";

    @ElementBy(id = "hipChatApiBaseUrl")
    private PageElement baseUrlInput;

    @ElementBy(id = "hipChatAuthToken")
    private PageElement tokenInput;

    @ElementBy(cssSelector = ".buttons-container input[type='submit']")
    private PageElement saveButton;

    @Override
    public TimedCondition isAt()
    {
        return baseUrlInput.timed().isPresent();
    }

    @Override
    public String getUrl()
    {
        return URI;
    }

    public HipChatConfigPage setToken(final String token)
    {
        tokenInput.clear().type(token);
        return this;
    }

    public HipChatConfigPage setApiBaseUrl(final String baseUrl){
        baseUrlInput.clear().type(baseUrl);
        return this;
    }

    public HipChatConfigPage save()
    {
        saveButton.click();
        return this;
    }
}
