package it.com.atlassian.labs.hipchat.pageobjects;


import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

public class HipChatNotificationsRow
{
    private final PageElement row;

    public HipChatNotificationsRow(final PageElement row)
    {
        this.row = row;
    }

    public String getWorkflowName()
    {
        return row.find(By.className("hipchat-notification-workflow")).getText();
    }

    public String getIssueActionName()
    {
        return row.find(By.className("hipchat-notification-action")).getText();
    }

    public void edit()
    {
        row.find(By.className("project-config-workflow-edit")).click();
    }

    public void remove()
    {
        row.find(By.className("project-config-workflow-remove")).click();
    }
}
