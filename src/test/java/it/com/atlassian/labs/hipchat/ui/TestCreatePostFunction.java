package it.com.atlassian.labs.hipchat.ui;


import org.junit.Test;

public class TestCreatePostFunction extends BaseTest
{
    @Test
    public void testCreatePostFunction() throws Exception
    {
        createHipChatPostFunction(WORKFLOW_NAME, "To Do", "Start Progress");
    }
}
